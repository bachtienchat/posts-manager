const express = require('express');
const router = express.Router();

//load model
const Post = require('../model/Post');

//display all posts
router.get('/', async (req,res) => {
  const posts = await Post.find().lean().sort({date: -1}) // this is array [
  //   {
  //     _id: new ObjectId("638f1661b3a6363095cb8669"),
  //     title: 'asds',
  //     text: 'ss',
  //     date: 2022-12-06T10:16:01.031Z,
  //     __v: 0
  //   },
  //   {
  //     _id: new ObjectId("638e20ddb3a6363095cb8664"),
  //     title: 'new post ',
  //     text: 'new post',
  //     date: 2022-12-05T16:48:29.781Z,
  //     __v: 0
  //   }
  // ]
  res.render('posts/displayPost', {posts})
})

//display form to create new post
router.get('/add', (req,res) => {
  res.render('posts/add')
});

//create new post
router.post('/', async (req,res) => {
  const { title, text } = req.body
  
  let errors = [];

  if(!title) errors.push({msg: 'Title required'})
  if(!text) errors.push({msg: 'Text required'})
  if(errors.length > 0) res.render('posts/add', {title, text})
  else {
    console.log({title, text})
    const newPostData = { title, text }
    const newPost = new Post(newPostData)
    await newPost.save()
    res.redirect('/posts')
  }
})

//display form to user edit post
router.get('/edit/:id', async (req,res) => {
  console.log(req)
  console.log(res)

  try {
    const post = await Post.findOne({ _id: req.params.id }).lean()
    res.render('posts/edit', { post })
    
  } catch (error) {
    console.log(error, 'mes')
  }
})

// update modifier post into database
router.put('/:id', async (req,res) => {
  try {
    const { title, text } = req.body;
    await Post.findOneAndUpdate({_id: req.params.id}, { title, text })
    res.redirect('/posts')
  } catch (error) {
    console.log(error, 'error')
  }
})

//delete post 
router.delete('/:id', async (req,res) => {
  try {
    await Post.findOneAndRemove({_id: req.params.id})
    res.redirect('/posts')
  } catch (error) {
    console.log(error, 'error')
  }
})
module.exports = router;